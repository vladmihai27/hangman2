package test;

import lasalle.Hangman.Word;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WordTest {
    Word word = new Word();
    @BeforeEach
    void setUp() {
        word.randomWordToGuess = word.guesses[0].toCharArray();
    }

    @Test
    void getWordLenght() {

        assertEquals(6, word.getWordLenght());
    }

    @Test
    void getWord() {
        String fb = "reddit";
        assertEquals(fb, word.getWord());
    }

    @Test
    void checkChar() {
        char input = 'd';
        int i = 2;
        assertTrue(word.checkChar(i, input));
    }
}